This script was written to bulk create filters in Jira.
Make sure to write your instance, api token, the number of filters you want to create, and the query for the filter.

If you have any other questions feel free to contact me at fbeck@atlassian.com

Fábio
