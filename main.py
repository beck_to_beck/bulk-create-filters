from library.api_calls import ApiCalls

def main():
    instance = 'https://<instance>.atlassian.net/'
    username = '<e-mail>'
    password = '<api_token>'

    # Set Authentication
    api_call = ApiCalls()
    api_call.setAuth('basic', username=username, password=password)

    # Set the number of filters
    number_of_filters = 100
    filter_queries = ['query 1', 'query 2', 'query 3']
    
    for x in range(0, number_of_filters):
        filter_data = {
            'name': 'new-filter ' + str(x + 1),
            # Set the filter query below
            'jql':  filter_queries[x]
        }

        response = api_call.request(
            'POST', instance + '/rest/api/3/filter', json_data=filter_data)
        print(str(x + 1),response.status_code)

if __name__ == '__main__':
    main()
